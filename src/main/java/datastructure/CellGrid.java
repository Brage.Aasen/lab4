package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    int rows;
    int columns;
    CellState[][] grid;


    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        grid = new CellState[rows][columns];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                grid[i][j] = initialState;
            }
        }
	}
    
    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        IGrid grid_copy =  new CellGrid(rows, columns, CellState.DEAD);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                grid_copy.set(i,j,get(i,j));
            }
        }
        return grid_copy;
    }
    
}
